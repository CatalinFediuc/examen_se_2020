package pachetExamen;

public class exercitiuldoi {
	public static void main(String[] args) {
		Executor ex;
		Thread t1;
		for(int count=0; count<6; count++) {
			if(count%2==0) {
				ex=new Executor(count+1);
				t1=new Thread(ex);
				t1.start();
				try {
					t1.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			else
				{
					System.out.println("SeThread1 - "+ (count+1));
					if(count!=5) {
						//make it not wait after last message
						try {
							Thread.sleep(5*1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
		}
	}
}
